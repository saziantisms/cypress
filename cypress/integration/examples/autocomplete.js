

it('TC00-Login',() => {
    
    cy.visit('https://regression-plus.solenovo.fi/education/app/educationgo/student/1842641934/personaldetails/default');
    //cy.clearCookie('JSESSIONID');
    cy.get('#username').type('studentatest');
    cy.get('#password').type('£xper!ment4l');
    cy.get('.btn-submit').click();
    cy.get('#root > div.layout.mainsidenav > div.main-container > div.page-content-area > div.content-container > div.content > form > fieldset:nth-child(4)').scrollIntoView();
    cy.get('#guardian-component span:nth-child(2)').click();
    cy.get('.position-relative > #parent').click();
    cy.get('.position-relative > #parent').type('ma');
    cy.get('.list-unstyled:nth-child(2) > .p-1').click();
    cy.get('.btn-dark > span').click();
    cy.get('fieldset:nth-of-type(2)').toMatchImageSnapshot({threshold: 0.001,})
    
    cy.get('#guardian-component > table.dataTable > tbody > tr:nth-of-type(1) > td.actions > svg:nth-of-type(2)').click();
    cy.get('div.form-actions > button:nth-of-type(2) > span:nth-of-type(2)').click();


});