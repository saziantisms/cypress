// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands'

// Alternatively you can use CommonJS syntax:
// require('./commands')
//import "cypress-xpath";
//add screenshot to Mochawesome

//cypress-plugin-snapshots
import 'cypress-plugin-snapshots/commands';
import addContext from "mochawesome/addContext";


Cypress.on("test:after:run",(test, runnable) => {
  if (test.state === "failed"){
    const screenshotFileName = `${runnable.parent.title} -- ${test.title} (failed).png`;
    C:\Users\Sazianti\CypressAutomation\cypress\cypress\integration\examples\__image_snapshots__
    const screenshotDiff = `${runnable.parent.title} -- ${test.title} (failed).png`;
    const video = `${Cypress.config('videosFolder')}/${Cypress.spec.name}.mp4`;
        addContext({ test }, `assets/${Cypress.spec.name}/${screenshotFileName}`);
        addContext({ test }, video);
        addContext({ test }, `assets/${Cypress.spec.name}/${screenshotFileName}`);
      
  }
});


